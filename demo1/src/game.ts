function createCabanaEntity() {
  let cabana = new Entity()
  cabana.add(new Transform({
    position: new Vector3(5, 0, 5),
    scale: new Vector3(0.005, 0.005, 0.005)
  }))

  let gltf = new GLTFShape('models/cabana/Cabana_2_and_Environment.gltf')
  cabana.add(gltf)

  return cabana
}

function createUmbrellaEntity() {
  let umbrella = new Entity()
  umbrella.add(new Transform({
    position: new Vector3(1, 0, 2),
    scale: new Vector3(0.6, 0.6, 0.6)
  }))

  let gltf = new GLTFShape('models/Umbrella_01/Umbrella_01.glb')
  umbrella.add(gltf)

  return umbrella
}

function createTreeEntity() {
  let tree = new Entity()
  tree.add(new Transform({
    position: new Vector3(8.5, 0, 8.5),
    scale: new Vector3(0.6, 0.6, 0.6)
  }))

  let gltf = new GLTFShape('models/TreeSycamore_02/TreeSycamore_02.glb')
  tree.add(gltf)

  return tree
}

function createPebble1Entity() {
  let pebble1 = new Entity()
  pebble1.add(new Transform({
    position: new Vector3(8, 0, 8),
    scale: new Vector3(1.5, 1.5, 1.5)
  }))

  const gltfPebble1 = new GLTFShape('models/Pebble_01/Pebble_01.glb')
  pebble1.add(gltfPebble1)

  return pebble1
}

function createPebble2Entity() {
  let pebble2 = new Entity()
  pebble2.add(new Transform({
    position: new Vector3(9, 0, 8.5),
    scale: new Vector3(1.5, 1.5, 1.5)
  }))

  const gltfPebble2 = new GLTFShape('models/Pebble_02/Pebble_02.glb')
  pebble2.add(gltfPebble2)

  return pebble2
}

function createDancingManEntity() {
  let dancingMan = new Entity()
  dancingMan.add(new Transform({
    position: new Vector3(4, 0, 1.5),
    scale: new Vector3(0.008, 0.008, 0.008)
  }))

  const gltf = new GLTFShape('models/dancing_man/Dancing_man.glb')
  dancingMan.add(gltf)

  dancingMan.get(GLTFShape).visible = false   //set dancing man invisible

  let dancingAnimation = dancingMan.get(GLTFShape).getClip("mixamo.com")
  dancingAnimation.play()   //play animation in invisible mode

  //add click event
  dancingMan.add(
    new OnClick(e => {
      log('play or pause dancing animation')
      if (dancingAnimation.playing) {
        dancingAnimation.pause()
      } else {
        dancingAnimation.play()
      }
    })
  )

  return dancingMan
}

const cabanaEntity = createCabanaEntity()
// const umbrellaEntity = createUmbrellaEntity()  // out of triangles
const treeEntity = createTreeEntity()
const pebble1Entity = createPebble1Entity()
const pebble2Entity = createPebble2Entity()
const dancingMan = createDancingManEntity()
engine.addEntity(cabanaEntity)
// engine.addEntity(umbrellaEntity)
engine.addEntity(treeEntity)
engine.addEntity(pebble1Entity)
engine.addEntity(pebble2Entity)
engine.addEntity(dancingMan)

pebble1Entity.add(
  new OnClick(e => {
    log('show dancing man.')
    dancingMan.get(GLTFShape).visible = !dancingMan.get(GLTFShape).visible
  })
)

pebble2Entity.add(
  new OnClick(e => {
    log('hide tree')
    treeEntity.get(GLTFShape).visible = !treeEntity.get(GLTFShape).visible
  })
)

