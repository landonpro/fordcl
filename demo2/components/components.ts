

@Component('entitySpin')
export class EntitySpin {
  active: boolean = false
  speed: number = 30
  direction: Vector3 = Vector3.Up()
}

@Component("lerpMoveData")
export class LerpMoveData {
	ismove: boolean =  false
  origin: Vector3 = Vector3.Zero()
  target: Vector3 = Vector3.Zero()
  speed: number = 2
  fraction: number = 0
}

@Component("lerpRotateData")
export class LerpRotateData {
	isrotate: boolean =  false
  originRot: Quaternion = Quaternion.Euler(0, 0, 0)
  targetRot: Quaternion = Quaternion.Euler(0, 180, 0)
  speed: number = 1
  fraction: number = 0
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function getCloseRotation(rotation){
  let x: number = 0
  let y: number = 0
  let z: number = 0
  if(rotation.eulerAngles.x%90<45){
    x = rotation.eulerAngles.x-(rotation.eulerAngles.x%90)
  }else{
    x = rotation.eulerAngles.x+90-(rotation.eulerAngles.x%90)
  }
  if(rotation.eulerAngles.y%90<45){
    y = rotation.eulerAngles.y-(rotation.eulerAngles.y%90)
  }else{
    y = rotation.eulerAngles.y+90-(rotation.eulerAngles.y%90)
  }
  if(rotation.eulerAngles.z%90<45){
    z = rotation.eulerAngles.z-(rotation.eulerAngles.z%90)
  }else{
    z = rotation.eulerAngles.z+90-(rotation.eulerAngles.z%90)
  }
  let targetRot: Quaternion = Quaternion.Euler(x,y,z)
  return targetRot
}


export function move(origin: Vector3,direction: Vector3, step: number){
  let des = new Vector3(0, 0, 0)
  des.x = origin.x + direction.x*step
  des.y = origin.y + direction.y*step
  des.z = origin.z + direction.z*step
  return des
}

export function checkTopBound(entity: Entity, x: number,y: number,z: number){
  let position = entity.get(Transform).position
  let xx = position.x
  let yy = position.y
  let zz = position.z
  if(xx>x||yy>y||zz>z){
    return false
  }else{
    return true
  }
}

export function checkBottomBound(entity: Entity, x: number,y: number,z: number){
  let position = entity.get(Transform).position
  let xx = position.x
  let yy = position.y
  let zz = position.z
  if(xx<x||yy<y||zz<z){
    return false
  }else{
    return true
  }
}

export function rotateAng(originrot: Quaternion, x: number,y: number,z: number){
  let angles = originrot.eulerAngles
  let targetRot: Quaternion = Quaternion.Euler(angles.x+x,angles.y+y,angles.z+z)
  return targetRot
}

export function turnLeft(entity: Entity, offset: number){
  let transform = entity.get(Transform)
  let position = transform.position
  let rotation = transform.rotation
  //
  let ang = 2*Math.PI/360* (rotation.eulerAngles.y+offset)
  let x = Math.sin(ang)
  let z = Math.cos(ang)
  let direction = new Vector3(x, 0, z)

  let rotDestination = rotateAng(rotation, 0, -90, 0)
  let moveingDirection = new Vector3(direction.x+direction.z, 0, direction.z-direction.x)
  let destination = move(position, moveingDirection, 2)

  //
  let slerp = entity.get(LerpRotateData)
  let lerp = entity.get(LerpMoveData)
 
  lerp.origin = position
  lerp.target = destination
  lerp.fraction = 0
  

  slerp.originRot = rotation
  slerp.targetRot = rotDestination
  slerp.fraction = 0

  slerp.isrotate = true
  lerp.ismove = true
}

export function turnRight(entity: Entity,offset: number){
  let transform = entity.get(Transform)
  let position = transform.position
  let rotation = transform.rotation

  let ang = 2*Math.PI/360* (rotation.eulerAngles.y+offset)
  let x = Math.sin(ang)
  let z = Math.cos(ang)
  let direction = new Vector3(x, 0, z)
  //
  let rotDestination = rotateAng(rotation, 0, 90, 0)
  let moveingDirection = new Vector3(direction.x-direction.z, 0, direction.z+direction.x)
  let destination = move(position, moveingDirection, 2)

  //
  let slerp = entity.get(LerpRotateData)
  let lerp = entity.get(LerpMoveData)
 
  lerp.origin = position
  lerp.target = destination
  lerp.fraction = 0
  

  slerp.originRot = rotation
  slerp.targetRot = rotDestination
  slerp.fraction = 0

  slerp.isrotate = true
  lerp.ismove = true
}

export function moveForward(entity: Entity, offset: number,step: number){
  let transform = entity.get(Transform)
  let rotation = transform.rotation
  let position = transform.position

  let ang = 2*Math.PI/360* (rotation.eulerAngles.y+offset)
  // log(rotation.eulerAngles.y-90)
  let x = Math.sin(ang)
  let z = Math.cos(ang)

  //
  let moveingDirection = new Vector3(x, 0, z)
  log(moveingDirection)
  let destination = move(position, moveingDirection, step)

  //
  let lerp = entity.get(LerpMoveData)
 
  lerp.origin = position
  lerp.target = destination
  lerp.fraction = 0
  
  lerp.ismove = true
}


export function moveBackward(entity: Entity,offset: number,step: number){
  let transform = entity.get(Transform)
  let position = transform.position
  let rotation = transform.rotation
  //
  let rotDestination = rotateAng(rotation, 0, 180, 0)
  let ang = 2*Math.PI/360* (rotDestination.eulerAngles.y+offset)
  let x = Math.sin(ang)
  let z = Math.cos(ang)

  let moveingDirection = new Vector3(x, 0, z)
  let destination = move(position, moveingDirection, step)

  //
  let slerp = entity.get(LerpRotateData)
  let lerp = entity.get(LerpMoveData)
 
  lerp.origin = position
  lerp.target = destination
  lerp.fraction = 0
  

  slerp.originRot = rotation
  slerp.targetRot = rotDestination
  slerp.fraction = 0

  slerp.isrotate = true
  lerp.ismove = true
}

export function stopMove(entity: Entity){
  let transform = entity.get(Transform)
  let position = transform.position
  let rotation = transform.rotation
  let slerp = entity.get(LerpRotateData)
  let lerp = entity.get(LerpMoveData)

  lerp.origin = position
  lerp.target = position
  lerp.fraction = 0
  

  slerp.originRot = rotation
  slerp.targetRot = getCloseRotation(rotation)
  slerp.fraction = 0

  lerp.ismove = false
}

const floorgltf = new GLTFShape('models/FloorBlock_01/FloorBlock_01.glb')
export function addFloors(x: number,z: number){
  for(let ii = 0; ii<x; ii++){
    for(let jj = 0; jj<z; jj++){
      let floor = new Entity() 
      floor.add(new Transform({
        position: new Vector3(4*ii+1, 0, 4*jj+1),
        scale: new Vector3(2, 2, 2)
      }))
      floor.add(floorgltf)
      engine.addEntity(floor)
    }
  }
}

export function makeCube(color: string, x: number,y: number, z: number){
  let position: Vector3 = new Vector3(x,y,z)
  const controlMaterial = new Material()
  let controlColor = Color3.FromHexString(color)
  controlMaterial.albedoColor = controlColor
  const cube = new Entity()
  const controlShape = new BoxShape()
  controlShape.withCollisions = true
  cube.add(controlMaterial)
  cube.add(new Transform({ position: position, scale: new Vector3(1,1,1) }))
  cube.add(controlShape)
  engine.addEntity(cube)
  return cube
}

const fencegltf = new GLTFShape('models/FenceWhitePicketLarge_02/FencePicketLarge_02.glb')
export function makeFence(x: number,y: number, z: number, angle: number){
  let fence = new Entity() 
  fence.add(new Transform({
    position: new Vector3(x, y, z),
    scale: new Vector3(1, 1, 1),
    rotation: Quaternion.Euler(0, angle, 0),
  }))
  fence.add(fencegltf)
  engine.addEntity(fence)
  return fence
}


export function makeText(yourtext: string,width: number, position: Vector3, scale: Vector3){
  const text = new Entity()
  const myText = new TextShape(yourtext)
  myText.width = width
  text.add(new Transform({ position: position, scale: scale }))
  text.add(myText)
  engine.addEntity(text)
  return text
}

const treegltf = new GLTFShape('models/TreeFir_02/TreeFir_02.glb')
export function makeTree(x: number,y: number, z: number, angle: number){
  let tree = new Entity() 
  tree.add(new Transform({
    position: new Vector3(x, y, z),
    scale: new Vector3(1, 1, 1),
    rotation: Quaternion.Euler(0, angle, 0),
  }))
  tree.add(treegltf)
  engine.addEntity(tree)
  return tree
}

const rockgltf = new GLTFShape('models/Rock_03/Rock_03.glb')
export function makeRock(x: number,y: number, z: number, angle: number){
  let rock = new Entity() 
  rock.add(new Transform({
    position: new Vector3(x, y, z),
    scale: new Vector3(1, 1, 1),
    rotation: Quaternion.Euler(0, angle, 0),
  }))
  rock.add(rockgltf)
  engine.addEntity(rock)
  return rock
}

const slidegltf = new GLTFShape('models/stair.glb')
export function makeSlide(x: number,y: number, z: number, angle: number){
  let slide = new Entity() 
  slide.add(new Transform({
    position: new Vector3(x, y, z),
    scale: new Vector3(5, 5, 5),
    rotation: Quaternion.Euler(0, angle, 0),
  }))
  slide.add(slidegltf)
  engine.addEntity(slide)
  return slide
}

const farmgltf1 = new GLTFShape('models/FarmVegetation_01/FarmVegetation_01.glb')
const farmgltf2 = new GLTFShape('models/FarmVegetation_02/FarmVegetation_02.glb')
const farmgltf3 = new GLTFShape('models/FarmVegetation_03/FarmVegetation_03.glb')
const farmgltf4 = new GLTFShape('models/FarmVegetation_04/FarmVegetation_04.glb')
export function makeFarm(x: number,y: number, z: number, angle: number, kind: number){
  let farm = new Entity() 
  farm.add(new Transform({
    position: new Vector3(x, y, z),
    scale: new Vector3(1, 1, 1),
    rotation: Quaternion.Euler(0, angle, 0),
  }))
  switch (kind) {
    case 1:
      farm.add(farmgltf1)
      break;
    case 2:
      farm.add(farmgltf2)
      break;
    case 3:
      farm.add(farmgltf3)
      break;
    case 4:
      farm.add(farmgltf4)
      break;
    default:
      farm.add(farmgltf4)
  }
  engine.addEntity(farm)
  return farm
}

const couchgltf1 = new GLTFShape('models/Couch_01/Couch_01.glb')
const couchgltf2 = new GLTFShape('models/Couch_02/Couch_02.glb')
const couchgltf3 = new GLTFShape('models/Couch_03/Couch_03.glb')

export function makeCouch(x: number,y: number, z: number, angle: number, kind: number){
  let couch = new Entity() 
  couch.add(new Transform({
    position: new Vector3(x, y, z),
    scale: new Vector3(1, 1, 1),
    rotation: Quaternion.Euler(0, angle, 0),
  }))
  switch (kind) {
    case 1:
      couch.add(couchgltf1)
      break;
    case 2:
      couch.add(couchgltf2)
      break;
    case 3:
      couch.add(couchgltf3)
      break;
    default:
      couch.add(couchgltf1)
  }
  engine.addEntity(couch)
  return couch
}

const chairgltf1 = new GLTFShape('models/Chair_01/Chair_01.glb')
const chairgltf2 = new GLTFShape('models/Chair_02/Chair_02.glb')
const chairgltf3 = new GLTFShape('models/Chair_03/Chair_03.glb')

export function makeChair(x: number,y: number, z: number, angle: number, kind: number){
  let chair = new Entity() 
  chair.add(new Transform({
    position: new Vector3(x, y, z),
    scale: new Vector3(1, 1, 1),
    rotation: Quaternion.Euler(0, angle, 0),
  }))
  switch (kind) {
    case 1:
      chair.add(chairgltf1)
      break;
    case 2:
      chair.add(chairgltf2)
      break;
    case 3:
      chair.add(chairgltf3)
      break;
    default:
      chair.add(chairgltf1)
  }
  engine.addEntity(chair)
  return chair
}

const tablegltf1 = new GLTFShape('models/Table_01/Table_01.glb')
const tablegltf2 = new GLTFShape('models/Table_02/Table_02.glb')
const tablegltf3 = new GLTFShape('models/Table_03/Table_03.glb')
const tablegltf4 = new GLTFShape('models/Table_04/Table_04.glb')

export function makeTable(x: number,y: number, z: number, angle: number, kind: number){
  let table = new Entity() 
  table.add(new Transform({
    position: new Vector3(x, y, z),
    scale: new Vector3(1, 1, 1),
    rotation: Quaternion.Euler(0, angle, 0),
  }))
  switch (kind) {
    case 1:
      table.add(tablegltf1)
      break;
    case 2:
      table.add(tablegltf2)
      break;
    case 3:
      table.add(tablegltf3)
      break;
    default:
      table.add(tablegltf4)
  }
  engine.addEntity(table)
  return table
}

