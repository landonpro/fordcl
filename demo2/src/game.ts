//===============carplay=================

import { LerpMoveData, LerpRotateData, EntitySpin, 
  turnLeft, turnRight, moveBackward, moveForward, 
  makeCube, makeFence, addFloors, stopMove, makeText, makeTree, 
  makeRock, makeFarm, makeCouch, makeChair, makeTable, makeSlide, checkTopBound, checkBottomBound} from '../components/components'
//===============components==============
// fences
makeFence(15,0,20,90)
makeFence(15,0,16,90)
makeFence(15,0,12,90)
makeFence(15,0,8,90)
makeFence(15,0,4,90)
makeFence(15,0,0,90)
//
makeFence(20,0,15,0)
makeFence(20,0,10,0)
makeFence(20,0,5,0)
makeFence(20,0,0,0)
// floors
addFloors(13,13)
// trees
makeTree(20,0,44,90)
makeTree(20,0,36,90)
makeTree(20,0,28,90)
makeTree(20,0,16,90)
makeTree(20,0,8,90)
makeTree(20,0,0,90)

makeTree(40,0,44,90)
makeTree(40,0,36,90)
makeTree(40,0,28,90)
makeTree(40,0,16,90)
makeTree(40,0,8,90)
makeTree(40,0,0,90)

makeTree(30,0,44,90)
makeTree(30,0,8,90)
makeTree(30,0,0,90)
// rocks
// makeRock(40*(Math.random()),0,40*(Math.random()),70)
// makeRock(40*(Math.random()),0,40*(Math.random()),70)
// makeRock(40*(Math.random()),0,40*(Math.random()),70)
// makeRock(40*(Math.random()),0,40*(Math.random()),70)
// makeRock(40*(Math.random()),0,40*(Math.random()),70)
// makeRock(40*(Math.random()),0,40*(Math.random()),70)
// makeRock(40*(Math.random()),0,40*(Math.random()),70)
// makeRock(40*(Math.random()),0,40*(Math.random()),70)
// makeRock(40*(Math.random()),0,40*(Math.random()),70)
// makeRock(40*(Math.random()),0,40*(Math.random()),70)
// makeFarm
makeFarm(20,0,32,90,1)
makeFarm(20,0,40,90,2)

makeFarm(40,0,32,90,3)
makeFarm(40,0,40,90,4)
makeFarm(40,0,20,90,1)
makeFarm(40,0,12,90,2)
makeFarm(40,0,4,90,3)
// makeFarm(40,0,24,90,2)

// makeFarm(25,0,0,0,4)
// makeFarm(35,0,0,0,1)
// makeFarm(25,0,8,0,2)
// makeFarm(35,0,8,0,3)

// makeFarm(25,0,44,0,4)
makeFarm(35,0,44,0,1)

// couch
makeCouch(34,0,13,180,1)
//chair
makeChair(33,0,16,180,1)
// table
makeTable(30,0,13,180,2)
// slide
// makeSlide(10,-6,30,-45)
// 
// makeCube("#CCCCCC",32,0,8)
// makeCube("#CCCCCC",32,0.5,9)
// makeCube("#CCCCCC",32,1,10)
// makeCube("#CCCCCC",32,1.5,11)
// makeCube("#CCCCCC",32,2,12)
// makeCube("#CCCCCC",32,2.5,13)
// makeCube("#CCCCCC",32,3,14)
// makeCube("#CCCCCC",32,3.5,15)

// makeCube("#CCCCCC",33,0,8)
// makeCube("#CCCCCC",33,0.5,9)
// makeCube("#CCCCCC",33,1,10)
// makeCube("#CCCCCC",33,1.5,11)
// makeCube("#CCCCCC",33,2,12)
// makeCube("#CCCCCC",33,2.5,13)
// makeCube("#CCCCCC",33,3,14)
// makeCube("#CCCCCC",33,3.5,15)
//======================================================
let car = new Entity() 
car.add(new Transform({
  position: new Vector3(5, 0, 5),
  scale: new Vector3(1, 1, 1),
  rotation: Quaternion.Euler(0, 90, 0),
}))
const cargltf = new GLTFShape('models/lamborghini.glb')
car.add(cargltf)
car.set(new LerpMoveData())
car.set(new LerpRotateData())
car.get(LerpRotateData).speed = 1/2
engine.addEntity(car)
// truck
let truck = new Entity() 
truck.add(new Transform({
  position: new Vector3(17, 0, 18),
  scale: new Vector3(0.01, 0.01, 0.01),
  rotation: Quaternion.Euler(0, 90, 0),
}))
const truckgltf = new GLTFShape('models/miningtruck.glb')
truck.add(truckgltf)
truck.set(new LerpMoveData())
truck.set(new LerpRotateData())
truck.add(
  new OnClick((e) => {
    // log('sssss')
    moveForward(truck,180,2)
    // turnLeft(truck,90)
  })
)
engine.addEntity(truck)
// spaceship
let spaceship = new Entity() 
spaceship.add(new Transform({
  position: new Vector3(15, 8, 10),
  scale: new Vector3(0.015, 0.015, 0.015),
  rotation: Quaternion.Euler(0, 180, 0),
}))
const spaceshipgltf = new GLTFShape('models/spaceship3.glb')
spaceship.add(spaceshipgltf)
spaceship.set(new LerpMoveData())
spaceship.set(new LerpRotateData())
spaceship.get(LerpRotateData).speed = 1/3
spaceship.add(
  new OnClick((e) => {
    if(checkTopBound(spaceship,50,40,50)&& checkBottomBound(spaceship,0,0,0)){
      moveForward(spaceship,180,10)
    }else{
      moveBackward(spaceship,180,10)
    }
  })
)
engine.addEntity(spaceship)
// house
let house = new Entity() 
house.add(new Transform({
  position: new Vector3(24, -6.02, 25),
  scale: new Vector3(5, 5, 5),
  rotation: Quaternion.Euler(0, 0, 0),
}))
const housegltf = new GLTFShape('models/house/model.gltf')
house.add(housegltf)
engine.addEntity(house)
//
// let ship = new Entity() 
// ship.add(new Transform({
//   position: new Vector3(55, 12, 30),
//   scale: new Vector3(8, 8, 8),
//   rotation: Quaternion.Euler(0, 0, 0),
// }))
// const shipgltf = new GLTFShape('models/ship.glb')
// ship.add(shipgltf)
// engine.addEntity(ship)


// base
// let base = new Entity() 
// base.add(new Transform({
//   position: new Vector3(10, 0, 10),
//   scale: new Vector3(1, 1, 1),
//   rotation: Quaternion.Euler(0, 0, 0),
// }))
// const basegltf = new GLTFShape('models/base.glb')
// base.add(basegltf)
// engine.addEntity(base)

// a system to carry out the movement
export class LerpMove {
  update(dt: number) {
    let carGroup = engine.getComponentGroup(LerpMoveData)
    for( let entity of carGroup.entities){
      let transform = entity.get(Transform)
      let lerp = entity.get(LerpMoveData)
      if (lerp.ismove && lerp.fraction < 1) {
        transform.position = Vector3.Lerp(
          lerp.origin,
          lerp.target,
          lerp.fraction
        )
        lerp.fraction += dt * lerp.speed
        // log(lerp.fraction)
      }
    }
  }
}

export class LerpRotate {
  update(dt: number) {
    let carGroup = engine.getComponentGroup(LerpRotateData)
    for( let entity of carGroup.entities){
      let transform = entity.get(Transform)
      let slerp = entity.get(LerpRotateData)
      if(slerp.isrotate && slerp.fraction < 1){
        slerp.fraction += dt * slerp.speed
        let rot = Quaternion.Slerp(slerp.originRot, slerp.targetRot, slerp.fraction)
        transform.rotation = rot   
      }
    }
  }
}

engine.addSystem(new LerpMove())
engine.addSystem(new LerpRotate())

// control cubes

let topCube = makeCube("#CCCCCC",2,3,2)
let downCube = makeCube("#CC0000",2,1,2)
let rightCube = makeCube("#00CC00",3,2,2)
let leftCube = makeCube("#0000CC",1,2,2)
let middleCube = makeCube("#000000",2,2,2)

middleCube.add(
  new OnClick((e) => {
    stopMove(car)
  })
)
leftCube.add(
  new OnClick((e) => {
    turnLeft(car,-90)
    log('1')
    let transform = car.get(Transform)
  })
)
rightCube.add(
  new OnClick((e) => {
    turnRight(car,-90)
    let transform = car.get(Transform)
  })
)
topCube.add(
  new OnClick((e) => {
    moveForward(car,-90,5)
    let transform = car.get(Transform)
  })
)
downCube.add(
  new OnClick((e) => {
    moveBackward(car,-90,2)
    let transform = car.get(Transform)
  })
)

// text
let pos = new Vector3(5, 3, 2)
let scale = new Vector3(1, 1, 1)
makeText("Play with your Lamborghini!",5,pos,scale)
let pos2 = new Vector3(2, 4, 2)
makeText("move forward",2,pos2,scale)
let pos3 = new Vector3(1, 3, 2)
makeText("turn left",1,pos3,scale)
let pos4 = new Vector3(3, 3, 2)
makeText("turn right",1,pos4,scale)
let pos5 = new Vector3(1, 1, 2)
makeText("backward",1,pos5,scale)

